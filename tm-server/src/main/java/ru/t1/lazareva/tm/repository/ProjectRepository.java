package ru.t1.lazareva.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.repository.IProjectRepository;
import ru.t1.lazareva.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

}
