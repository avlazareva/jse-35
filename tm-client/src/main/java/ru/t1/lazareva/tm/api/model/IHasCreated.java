package ru.t1.lazareva.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasCreated {

    @Nullable
    Date getCreated();

    @SuppressWarnings("unused")
    void setCreated(@Nullable Date created);

}